    // const userName = prompt('Enter your name');
    // document.write(userName);

    // fetch('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
    // .then(data => data.json())
    // .then(currency => {
    //   const ul = document.createElement('ul');
    //   document.body.append(ul);
    //   currency.forEach((item) => {
    //     const li = document.createElement('li');
    //     li.innerHTML = `Name: ${item.txt} | Rate: ${item.rate}`
    //       ul.append(li);
    //   });
    // });
    const nav = document.querySelector('nav');
    const navHeight = nav.getBoundingClientRect().height;
    const modalLinkButton = $('.btn--link');

    $('.nav-link').click(function() {
      window.scrollTo({
        top: $(this.hash).offset().top,
        behavior: "smooth"
      });
      const navMenu = document.querySelector('#navbarNav');
      navMenu.classList.remove('show');
      return false;
    });

    window.onscroll = function() {
      if(window.pageYOffset > navHeight) {
        nav.classList.add('bg-gray');
      } else {
        nav.classList.remove('bg-gray');
      }
    }

    $('body').scrollspy({ target: '#header-nav' });

    $('.btn-open-modal').click(function() {
      const parentDiv = $(this).closest('.carousel-item')[0];
      const parentDivData = parentDiv.dataset;

      $('#exampleModalLongTitle').text(parentDivData.carName);
      $('#exampleModalLongDescription').text(parentDivData.carDescription);

      modalLinkButton.click(function() {
        window.location.href = parentDivData.carLink;
      });
    });
